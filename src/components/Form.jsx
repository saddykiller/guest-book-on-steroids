import React from 'react';
import PropTypes from 'prop-types';
import Big from 'big.js';
import SignIn from './SignIn';

export default function Form({ onSubmit, currentUser, onSignChange, signNum, hadMsg }) {
  return (
    <form onSubmit={onSubmit}>
      <fieldset id="fieldset">
        <p>Sign the guest book, { currentUser.accountId }!</p>
        <p className="highlight">
          <input
            placeholder="Enter your message here"
            autoComplete="off"
            autoFocus
            id="message"
            required
          />
        </p>
        <p>
          <label htmlFor="donation">Donation:</label>
          <input
            autoComplete="off"
            defaultValue={'0'}
            id="donation"
            max={Big(currentUser.balance).div(10 ** 24)}
            min="0"
            step="0.01"
            type="number"
          />
          <span title="NEAR Tokens">Ⓝ</span>
        </p>

        <p>
          <label htmlFor="numSigns">Signs:</label>
          <input
            value={signNum}
            onChange={onSignChange}
            autoComplete="off"
            id="numSigns"
            max="100"
            min="0"
            step="1"
            type="number"
          />
        </p>
        {hadMsg ? 
          <p>Sory, sign blocked, you already had message.</p>
          :
          <button type="submit">
            Sign
          </button>                
        } 
      </fieldset>
    </form>
  );
}

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired
  })
};
