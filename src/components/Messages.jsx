import React from 'react';
import PropTypes from 'prop-types';

export default function Messages({ messages }) {
  console.log(messages[0])
  return (
    <>
      <h2>Messages</h2>
      {messages.map((message, i) =>
        // TODO: format as cards, add timestamp
        
        <p key={i} className={'signCard ' + (message.premium ? 'is-premium' : '')}>
          <strong>{message.sender}</strong>:<br/>
          {message.text} at {new Date(message.timestamp).toUTCString()}
        </p>
      )}
    </>
  );
}

Messages.propTypes = {
  messages: PropTypes.array
};
