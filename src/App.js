import 'regenerator-runtime/runtime';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Big from 'big.js';
import Form from './components/Form';
import SignIn from './components/SignIn';
import Messages from './components/Messages';

const SUGGESTED_DONATION = '0';
const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();

const App = ({ contract, currentUser, nearConfig, wallet }) => {
  const [messages, setMessages] = useState([]);
  const [signNum, setSignNum] = useState(4);
  const [hadMsg, setHadMsg] = useState(false)

  const checkMsg = () => {
    if (!currentUser) { return;}
    contract.getAllMessages().then(allMessages => {
      for(let i = 0; i < allMessages.length; i++) {
        if (allMessages[i].sender === currentUser.accountId) {
          setHadMsg(true);
          break;
        }
      } 
    })
  }

  useEffect(() => {
    // TODO: don't just fetch once; subscribe!
    contract.getMessages({numOfMsg: signNum}).then(setMessages);
    checkMsg();

  }, []);

  const onSignChange = (e) => {
    e.preventDefault();
    let numberOfSigns = parseInt(e.target.value);
    setSignNum(numberOfSigns);
    contract.getMessages({numOfMsg: numberOfSigns}).then(setMessages);
    checkMsg();

  }

  const onSubmit = (e) => {
    e.preventDefault();
    if (hadMsg) {
      return;
    }
    const { fieldset, message, donation } = e.target.elements;

    fieldset.disabled = true;
    // TODO: optimistically update page with new message,
    // update blockchain data in background
    // add uuid to each message, so we know which one is already known
    contract.addMessage(
      { text: message.value},
      BOATLOAD_OF_GAS,
      Big(donation.value || '0').times(10 ** 24).toFixed()
    ).then(() => {
      console.log("Sending number of sign:", signNum);
      contract.getMessages({numOfMsg: signNum}).then(messages => {
        setMessages(messages);
        message.value = '';
        donation.value = SUGGESTED_DONATION;
        fieldset.disabled = false;
        checkMsg();
        message.focus();
      });
    });
  };

  const signIn = () => {
    wallet.requestSignIn(
      nearConfig.contractName,
      'NEAR Guest Book'
    );
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <main>
      <header>
        <h1>NEAR Guest Book</h1>
        { currentUser
          ? <button onClick={signOut}>Log out</button>
          : <button onClick={signIn}>Log in</button>
        }
      </header>
      { currentUser
        ? <Form onSubmit={onSubmit} currentUser={currentUser} onSignChange={onSignChange} signNum={signNum} hadMsg={hadMsg}/>
        : <SignIn/>
      }
      { !!currentUser && !!messages.length && <Messages messages={messages}/> }
    </main>
  );
};

App.propTypes = {
  contract: PropTypes.shape({
    addMessage: PropTypes.func.isRequired,
    getMessages: PropTypes.func.isRequired
  }).isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired
  }),
  nearConfig: PropTypes.shape({
    contractName: PropTypes.string.isRequired
  }).isRequired,
  wallet: PropTypes.shape({
    requestSignIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired
  }).isRequired
};

export default App;
