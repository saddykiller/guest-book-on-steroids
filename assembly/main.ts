import { PostedMessage, messages } from './model';
import { context } from "near-sdk-as";

// --- contract code goes below

// The maximum number of latest messages the contract returns.
const MESSAGE_LIMIT = 10;

/**
 * Adds a new message under the name of the sender's account id.\
 * NOTE: This is a change method. Which means it will modify the state.\
 * But right now we don't distinguish them with annotations yet.
 */
export function addMessage(text: string): void {
  // Creating a new message and populating fields with our data
  const message = new PostedMessage(text);
  // Adding the message to end of the the persistent collection
  messages.push(message);
}

/**
 * Returns an array of last N messages.\
 * NOTE: This is a view method. Which means it should NOT modify the state.
 */
export function getMessages(numOfMsg? : i32): PostedMessage[] {
  const msgToShow = numOfMsg || MESSAGE_LIMIT;
  const numMessages = min(msgToShow, messages.length);
  const startIndex = messages.length - numMessages;
  const result = new Array<PostedMessage>(numMessages);
  for(let i = 0; i < numMessages; i++) {
    result[i] = messages[i + startIndex];
  }
  return result;
}

/**
 * Returns true if sender allready have mesage\
 * NOTE: This is a view method. Which means it should NOT modify the state.
 */
 export function getAllMessages(): PostedMessage[] {
  const result = new Array<PostedMessage>(messages.length);
  for(let i = 0; i < messages.length; i++) {
    result[i] = messages[i];
  }
  return result;
}